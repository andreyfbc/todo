import { LightningElement, api } from 'lwc';
import SUBTODO_TODO_FIELD from '@salesforce/schema/Sub_ToDo__c.ToDo__c';
import SUBTODO_NAME_FIELD from '@salesforce/schema/Sub_ToDo__c.Name';
import SUBTODO_DESCRIPTION_FIELD from '@salesforce/schema/Sub_ToDo__c.Description__c';
import SUBTODO_DATE_FIELD from '@salesforce/schema/Sub_ToDo__c.Due_Date__c';
import SUBTODO_PRIORITY_FIELD from '@salesforce/schema/Sub_ToDo__c.Priority__c';
import SUBTODO_STATUS_FIELD from '@salesforce/schema/Sub_ToDo__c.Status__c';
import SUBTODO_COMMENT_FIELD from '@salesforce/schema/Sub_ToDo__c.Comment__c';

export default class SubtodoItem extends LightningElement {
    subtodoTodoField = SUBTODO_TODO_FIELD;
    subtodoNameField = SUBTODO_NAME_FIELD;
    subtodoDescriptionField = SUBTODO_DESCRIPTION_FIELD;
    subtodoDateField = SUBTODO_DATE_FIELD;
    subtodoPriorityField = SUBTODO_PRIORITY_FIELD;
    subtodoStatusField = SUBTODO_STATUS_FIELD;
    subtodoCommentField = SUBTODO_COMMENT_FIELD;
    @api selectedTodo;
}
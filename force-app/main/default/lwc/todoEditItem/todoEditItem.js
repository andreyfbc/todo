import { LightningElement, track, api} from 'lwc';
import TODO_OBJECT from '@salesforce/schema/ToDo__c';
import NAME_FIELD from '@salesforce/schema/ToDo__c.Name';
import DESCRIPTION_FIELD from '@salesforce/schema/ToDo__c.Description__c';
import DATE_FIELD from '@salesforce/schema/ToDo__c.Due_Date__c';
import RECORD_TYPE_FIELD from '@salesforce/schema/ToDo__c.RecordTypeId';
import PRIORITY_FIELD from '@salesforce/schema/ToDo__c.Priority__c';
import STATUS_FIELD from '@salesforce/schema/ToDo__c.Status__c';
import OWNER_FIELD from '@salesforce/schema/ToDo__c.OwnerId'; 
import COMMENT_FIELD from '@salesforce/schema/ToDo__c.Comment__c';
import PRIORITY_FLAG_FIELD from '@salesforce/schema/ToDo__c.PriorityFlag__c';

export default class TodoEditItem extends LightningElement {
    @api selectedTodo;
    @track objectApiName = TODO_OBJECT;

    nameField = NAME_FIELD;
    descriptionField = DESCRIPTION_FIELD;
    dateField = DATE_FIELD;
    ecordTypeField = RECORD_TYPE_FIELD;
    priorityField = PRIORITY_FIELD;
    statusField = STATUS_FIELD;
    commentField = COMMENT_FIELD;
    ownerField = OWNER_FIELD;
   
}
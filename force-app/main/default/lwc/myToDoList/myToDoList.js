import { LightningElement, wire, track, api } from 'lwc';
import getToDosForTree from '@salesforce/apex/TodoController.getToDosForTree';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';

import TODO_OBJECT from '@salesforce/schema/ToDo__c';
import SUBTODO_OBJECT from '@salesforce/schema/Sub_ToDo__c';

export default class myToDoList extends LightningElement {
    selectedTodo;
    selectedSubTodo;
    @track todoes;
    openModal = false;
    openModalSubtodo = false;
    @track objectApiName = TODO_OBJECT;
    @track isTodo = true;

    @wire(getToDosForTree) 
    wireTodoData({error, data}){
        if(data){
            this.todoes = data;
        }
        else{
            this.error = error;
        }
    }  
    
    handleTodoCreation(event) {
        //console.log("******" + event.detail.id);
        if(event.detail.id !==null){
            this.openModal = false;
            this.dispatchEvent(new ShowToastEvent({
                title: "SUCCESS!",
                message: "New ToDo record has been created.",
                variant: "success",
            }),
        );
        return refreshApex(this.todoes);
        }
    }

    handleSubTodoCreation(event) {
        if(event.detail.id!==null){
            this.openModalSubtodo = false;
            this.dispatchEvent(new ShowToastEvent({
                title: "SUCCESS!",
                message: "New SubToDo record has been created.",
                variant: "success",
            }),
        );
        return refreshApex(this.todoes);
        }
    }

    handleSelect(event) {
        const todo = event.detail.name;
        if(todo){
             const f = this.setApiName(this.todoes, 'name', todo);
             if (this.isTodo ===true){
                this.selectedTodo = todo; 
                this.selectedSubTodo = "";
             }                
             else{
                this.selectedSubTodo = todo; 
                this.selectedTodo = "";
            }
        }    
     }

    showModal(){
        this.openModal = true;
    }
   
    closeModal(){
        this.openModal = false;
    }

    showModalSubTodo(){
        this.openModalSubtodo = true;
    }

    closeModalSubTodo(){
        this.openModalSubtodo = false;
    }

    setApiName(obj, key, value) {
        if (obj[key] === value && !obj.isTodo) {
            this.objectApiName = SUBTODO_OBJECT;
            this.isTodo = false;
            return obj.isTodo;
        }
              
        else if (obj[key] === value && obj.isTodo) {
            this.objectApiName = TODO_OBJECT;
            this.isTodo = true;
            return obj.isTodo;
        }
               
        const objKeys = Object.keys(obj);
        for (const k of objKeys) {
            if (typeof obj[k] === 'object' || Array.isArray(obj[k])) {
                const found = this.setApiName(obj[k], key, value);
                if (found) {
                    return found;
                }
            }
        }

       return null;
    }

    refreshList(event) {
        return refreshApex(this.todoes);
    }
}
public with sharing class ToDoTriggerHandler {

    public static void handlerBeforeInsert(List<ToDo__c> newList) {
         ToDoHelper.addToDoToQueu(newList);
     }
     
  }
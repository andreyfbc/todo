/**
 * Implement REST web service with @HttpGet, @HttpPost, @HttpDelete,
 * @HttpPut, @HttpPatch methods for the ToDo__c object (get, create, delete, upsert, update ToDo record)
 */
@RestResource(UrlMapping='/ToDo__c/*') 
global with sharing class ToDoManager {
    @HttpGet
    global static ToDo__c getToDoById() {
        RestRequest request = RestContext.request;
        String ToDoId = request.requestURI.substring(
                request.requestURI.lastIndexOf('/') + 1);
        List<ToDo__c> todos = [
                SELECT Id
                FROM ToDo__c
                WHERE Id = :ToDoId
        ];
        ToDo__c result = todos.get(0);
        return result;
    }

    @HttpPost
    global static ID createToDo(String name, String description, String priority, String recordType, Datetime dueDate) {
        ToDo__c thisToDo = new ToDo__c(
                Name = name,
                Description__c = description,
                Priority__c = priority,
                RecordTypeId = recordType,
                Due_Date__c = dueDate);
        insert thisToDo;
        return thisToDo.Id;
    }

    @HttpDelete
    global static void deleteToDo() {
        RestRequest request = RestContext.request;
        String ToDoId = request.requestURI.substring(
                request.requestURI.lastIndexOf('/') + 1);
        ToDo__c thisToDo = [
                SELECT Id 
                FROM ToDo__c 
                WHERE Id = :ToDoId
        ];
        delete thisToDo;
    }

    @HttpPut
    global static ID upsertToDo(String name, String description,  
                                String priority, Id recordType, 
                                Datetime dueDate, String id) {
        ToDo__c thisToDo = new ToDo__c(
                Id = id,
                Name = name,
                Description__c = description,
                Priority__c = priority,
                RecordTypeId = recordType,
                Due_Date__c = dueDate);
        // Match ToDo by Id, if present.
        // Otherwise, create new ToDo__c.
        upsert thisToDo;
        // Return the ToDo ID.
        return thisToDo.Id;
    }

    @HttpPatch
    global static ID updateToDoFields() {
        RestRequest request = RestContext.request;
        String ToDoId = request.requestURI.substring(
                request.requestURI.lastIndexOf('/') + 1);
        ToDo__c thisToDo = [
            SELECT Id 
            FROM ToDo__c 
            WHERE Id = :ToDoId
        ];
        // Deserialize the JSON string into name-value pairs
        Map<String, Object> params = (Map<String, Object>) JSON.deserializeUntyped(request.requestbody.tostring());
        // Iterate through each parameter field and value
        for (String fieldName : params.keySet()) {
            // Set the field and value on the ToDo sObject
            thisToDo.put(fieldName, params.get(fieldName));
        }
        update thisToDo;
        return thisToDo.Id;
    }
}
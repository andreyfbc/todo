@IsTest
public with sharing class ToDoCalloutsTest {
    static final String TEST_BODY = '{"name": "Check web-service", "Description__c": "test callout from 1 org to another"}';
    static final String TEST_ID = '0010900000bbqIMAAY';

    @IsTest
    static void testGetCallout() {
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('GetTodoResource');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json;charset=UTF-8');

        // Associate the callout with a mock response
        Test.setMock(HttpCalloutMock.class, mock);
        // Call method to test
        HttpResponse result = ToDoCallouts.makeGetCallout(TEST_ID);
        // Verify mock response is not null
        System.assertNotEquals(null, result, 'The callout returned a null response.');

        // Verify status code
        System.assertEquals(200, result.getStatusCode(), 'The status code is not 200.');

        // Verify content type
        System.assertEquals('application/json;charset=UTF-8',
                result.getHeader('Content-Type'),
                'The content type value is not expected.');

        // Verify the array contains 3 items
        Map<String, Object> results = (Map<String, Object>)
                JSON.deserializeUntyped(result.getBody());
        //List<Object> todos = (List<Object>) results.values();

        List<Object> todos = (List<Object>) results.get('Todos');
        for (Object todo : todos) {
            System.debug('Todo name ==' + todo);

        }
        System.assertEquals(3, todos.size(), 'The array of todos should only contain 3 items.');
    }

    @IsTest
    static void testPostCallout() {
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new TodoHttpCalloutMock());
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock.
        HttpResponse response = ToDoCallouts.makePostCallout(TEST_BODY);
        // Verify that the response received contains fake values
        String contentType = response.getHeader('Content-Type');
        System.assert(contentType == 'application/json');
        String actualValue = response.getBody();
        System.debug(response.getBody());
        String expectedValue = TEST_BODY;
        System.assertEquals(actualValue, expectedValue);
        System.assertEquals(200, response.getStatusCode());
    }

    @IsTest
    static void testDeleteCallout() {
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('GetTodoResource');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json;charset=UTF-8');

        // Associate the callout with a mock response
        Test.setMock(HttpCalloutMock.class, mock);
        // Call method to test
        HttpResponse result = ToDoCallouts.makeDeleteCallout(TEST_ID);
        // Verify mock response is not null
        System.assertNotEquals(null, result, 'The callout returned a null response.');

        // Verify status code
        System.assertEquals(200, result.getStatusCode(), 'The status code is not 200.');

        // Verify content type
        System.assertEquals('application/json;charset=UTF-8',
                result.getHeader('Content-Type'),
                'The content type value is not expected.');

        List<ToDo__c> todos = [SELECT Id FROM ToDo__c WHERE Id = :TEST_ID];
        System.assertEquals(0, todos.size(), 'After delete the array of todos should only contain 0 items.');

    }

    @IsTest
    static void testPatchCallout() {
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('GetTodoResource');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json;charset=UTF-8');

        // Associate the callout with a mock response
        Test.setMock(HttpCalloutMock.class, mock);
        // Call method to test
        HttpResponse result = ToDoCallouts.makePatchCallout(TEST_ID, TEST_BODY);

        System.assertNotEquals(null, result, 'The callout returned a null response.');
        System.assertEquals(200, result.getStatusCode(), 'The status code is not 200.');
        System.assertEquals('application/json;charset=UTF-8',
                result.getHeader('Content-Type'),
                'The content type value is not expected.');

        // Verify the array contains 3 items
        Map<String, Object> results = (Map<String, Object>)
                JSON.deserializeUntyped(result.getBody());
        //List<Object> todos = (List<Object>) results.values();
        List<Object> todos = (List<Object>) results.get('Todos');
        System.assertEquals(3, todos.size(), 'The array of todos should only contain 3 items.');
    }

    @IsTest
    static void testPutCallout() {
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new TodoHttpCalloutMock());
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock.
        HttpResponse response = ToDoCallouts.makePutCallout(TEST_ID, TEST_BODY);
        // Verify that the response received contains fake values
        String contentType = response.getHeader('Content-Type');
        System.assert(contentType == 'application/json');
        String actualValue = response.getBody();
        System.debug(response.getBody());
        String expectedValue = TEST_BODY;
        System.assertEquals(actualValue, expectedValue);
        System.assertEquals(200, response.getStatusCode());
    }

    @IsTest
    static void testGetCalloutWRONG() {
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('GetTodoResource');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json;charset=UTF-8');

        // Associate the callout with a mock response
        Test.setMock(HttpCalloutMock.class, mock);
        // Call method to test
        HttpResponse result = ToDoCallouts.makeGetCallout(TEST_ID);
        // Verify mock response is not null
        System.assertNotEquals(null, result, 'The callout returned a null response.');

        // Verify status code
        System.assertEquals(200, result.getStatusCode(), 'The status code is not 200.');

        // Verify content type
        System.assertEquals('application/json;charset=UTF-8',
                result.getHeader('Content-Type'),
                'The content type value is not expected.');

        Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(
                result.getBody());

        Map<String, Object> error = (Map<String, Object>) m.get('error');
        if (error != null) {
            System.debug(string.valueOf(error.get('message')));
        }

        List<Map<String, Object>> rows = new List<Map<String, Object>>();
        List<Object> jsonRows = (List<Object>) m.get('Name');
        if (jsonRows == null) {
            rows.add(foundRow(m));
            System.debug('method get returned empty result');
        } else {
            for (Object jsonRow : jsonRows) {
                Map<String, Object> row = (Map<String, Object>) jsonRow;
                rows.add(foundRow(row));
                System.debug('Todo name ==' + jsonRow);
            }
        }

    }

    // Populate a row based on values from the external system.
    private static Map<String, Object> foundRow(Map<String, Object> foundRow) {
        Map<String, Object> row = new Map<String, Object>();
        row.put('ExternalId', string.valueOf(foundRow.get('Id')));
        row.put('DisplayUrl', string.valueOf(foundRow.get('DisplayUrl')));
        row.put('Name', string.valueOf(foundRow.get('Name')));
        return row;
    }
}
global with sharing class CleanOldTodoSchedule implements Schedulable {
    global void execute(SchedulableContext SC){
        Database.executeBatch(new CleanOldTodoBatch(), 200);
    }
}

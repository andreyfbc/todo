@IsTest
private class ToDoTriggerTest {
    @IsTest
    static void testaddToDoToQueu() {
        List<Todo__c> todos = new List<Todo__c>();

        Map <String,Schema.RecordTypeInfo> recordTypesTodo = Todo__c.sObjectType.getDescribe().getRecordTypeInfosByName();
        
        //create 3 toDos
        Todo__c todoBusiness = TestDataFactory.TODO.createDummyTodo('Test todo business', ToDoNames.TO_DO_BUSINESS_RECORD_TYPE, 'Low', false);
        Todo__c todoCorporate = TestDataFactory.TODO.createDummyTodo('Test todo corporate', ToDoNames.TO_DO_CORPORATE_RECORD_TYPE, 'High', false);
        Todo__c todoPersonal = TestDataFactory.TODO.createDummyTodo('Test todo personal', ToDoNames.TO_DO_PERSONAL_RECORD_TYPE, 'Low', false);
       
        todos.add(todoBusiness);
        todos.add(todoCorporate);
        todos.add(todoPersonal);
        
        Test.startTest();
        insert todos;
        Test.stopTest();

        List<Id> ids = new List<Id>();
        ids.add(todoBusiness.Id);
        ids.add(todoCorporate.Id);
        ids.add(todoPersonal.Id);
        
        List<Todo__c> newTodos = [
            SELECT Name, 
                    OwnerId, 
                    RecordTypeId 
            FROM Todo__c 
            WHERE Id in:ids 
        ];

        for (Todo__c todo : newTodos) {
            if (toDo.RecordTypeId!=ToDoNames.TO_DO_PERSONAL_RECORD_TYPE) {
                System.assertEquals(todo.OwnerId, ToDoNames.QUEUE_RECORD_TYPE_MAP.get(toDo.RecordTypeId));
            }    
            else {
                System.assertEquals(todo.OwnerId, UserInfo.getUserId());
            }
        }
    }
}
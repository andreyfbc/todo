public with sharing class ToDoNames {
   
   public static Id TO_DO_BUSINESS_RECORD_TYPE{
        get{
            if(TO_DO_BUSINESS_RECORD_TYPE == null){
                    RecordType todoRecTypeId = [
                        SELECT Id 
                        FROM RecordType 
                        WHERE sObjectType = 'ToDo__c' and DeveloperName ='Business'
                    ];
                    TO_DO_BUSINESS_RECORD_TYPE = todoRecTypeId.Id;
            }
            return TO_DO_BUSINESS_RECORD_TYPE;
        }
        private set;
   }

   public static Id TO_DO_PERSONAL_RECORD_TYPE{
        get{
            if(TO_DO_PERSONAL_RECORD_TYPE == null){
                    RecordType todoRecTypeId = [
                        SELECT Id 
                        FROM RecordType 
                        WHERE sObjectType = 'ToDo__c' and DeveloperName ='Personal'
                    ];
                    TO_DO_PERSONAL_RECORD_TYPE = todoRecTypeId.Id;
            }
            return TO_DO_PERSONAL_RECORD_TYPE;
        }
        private set;
    }

    public static Id TO_DO_CORPORATE_RECORD_TYPE{
        get{
            if(TO_DO_CORPORATE_RECORD_TYPE == null){
                    RecordType todoRecTypeId = [
                        SELECT Id 
                        FROM RecordType 
                        WHERE sObjectType = 'ToDo__c' and DeveloperName ='Corporate'
                    ];
                    TO_DO_CORPORATE_RECORD_TYPE = todoRecTypeId.Id;
            }
            return TO_DO_CORPORATE_RECORD_TYPE;
        }
        private set;
    }

    public static Map<Id,Id> QUEUE_RECORD_TYPE_MAP{
        get{
            if(QUEUE_RECORD_TYPE_MAP == null){
                QUEUE_RECORD_TYPE_MAP = new Map<Id,Id>();
                List<Group> groups = [
                    SELECT Id, 
                            Name 
                    FROM Group 
                    WHERE  Type = 'Queue' 
                        AND (NAME = 'ToDo_Support_Tier_1' 
                                OR NAME = 'ToDo_Support_Tier_2')
                ];
                
                for (Group queueGroup : groups) {
                    if (queueGroup.Name =='ToDo_Support_Tier_1') {
                        QUEUE_RECORD_TYPE_MAP.put(TO_DO_BUSINESS_RECORD_TYPE, queueGroup.Id); 
                    }
                    else if (queueGroup.Name =='ToDo_Support_Tier_2'){
                        QUEUE_RECORD_TYPE_MAP.put(TO_DO_CORPORATE_RECORD_TYPE, queueGroup.Id);   
                    }
                }
            }
            return QUEUE_RECORD_TYPE_MAP;
        }
        private set;
    }    
}
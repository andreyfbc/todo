@IsTest
private class ToDoManagerTest {

    @isTest
    static void testgetToDoById() {
        Id recordId = createTestRecord();
        // Set up a test request
        RestRequest request = new RestRequest();
        request.requestUri =
                'https://d09000007zhlleam-dev-ed.lightning.force.com/lightning/r/ToDo__c/'
                        + recordId;

        request.httpMethod = 'GET';
        RestContext.request = request;
        ToDo__c thisToDo = ToDoManager.getToDoById();
        System.assert(thisToDo != null);
        System.assertEquals('Check tasks', thisToDo.Name);
        System.assertEquals('Only for legal department', thisToDo.Description__c);
    }

    @isTest
    static void testcreateToDo() {
        DateTime toDoDateTime = DateTime.newInstance(2021, 9, 30, 22, 0, 0);
        
        ID thisToDoId = ToDoManager.createToDo(
                'Test toDo', 'Test creating toDo', 'Low', ToDoNames.TO_DO_BUSINESS_RECORD_TYPE, toDoDateTime);
        // Verify results
        System.assert(thisToDoId != null);
       
        ToDo__c thisToDo = [
                SELECT Id,
                        Name, 
                        Description__c, 
                        Priority__c, 
                        Due_Date__c, 
                        RecordTypeId
                FROM ToDo__c
                WHERE Id = :thisToDoId
        ];
       
        System.assert(thisToDo != null);
        System.assertEquals(thisToDo.Name, 'Test toDo');
        System.assertEquals(thisToDo.Description__c, 'Test creating toDo');
        System.assertEquals(thisToDo.Priority__c, 'Low');
        System.assertEquals(thisToDo.RecordTypeId, ToDoNames.TO_DO_BUSINESS_RECORD_TYPE);
        System.assertEquals(thisToDo.Due_Date__c, toDoDateTime);
    }

    @isTest
    static void testdeleteToDo() {
        Id recordId = createTestRecord();
        // Set up a test request
        RestRequest request = new RestRequest();
        request.requestUri =
                'https://d09000007zhlleam-dev-ed.lightning.force.com/lightning/r/ToDo__c/'
                        + recordId;
        request.httpMethod = 'GET';
        RestContext.request = request;
        // Call the method to test
        ToDoManager.deleteToDo();
        // Verify record is deleted
        List<ToDo__c> toDos = [
                SELECT Id 
                FROM ToDo__c 
                WHERE Id = :recordId
        ];
        System.assert(toDos.size() == 0);
    }

    @isTest
    static void testupsertToDo() {
        DateTime toDoDateTime = DateTime.newInstance(2021, 9, 30, 22, 0, 0);
        
        // 1. Insert new record
        ID toDo1Id = ToDoManager.upsertToDo(
                'Fill report', 'for today', 'High', ToDoNames.TO_DO_CORPORATE_RECORD_TYPE, toDoDateTime, null);
        // Verify new record was created
        System.assert(toDo1Id != null);
        ToDo__c toDo1 = [
                SELECT Id,
                        Name 
                FROM ToDo__c 
                WHERE Id = :toDo1Id
        ];
        System.assert(toDo1 != null);
        System.assertEquals(toDo1.Name, 'Fill report');
        
        // 2. Update description of existing record to ***
        ID toDo2Id = ToDoManager.upsertToDo(
                'Fill report', 'description was updated', 'High', ToDoNames.TO_DO_CORPORATE_RECORD_TYPE, toDoDateTime, toDo1Id);
        // Verify record was updated
        System.assertEquals(toDo1Id, toDo2Id);
        ToDo__c toDo2 = [
                SELECT Id, 
                        Description__c 
                FROM ToDo__c 
                WHERE Id = :toDo2Id
        ];
        System.assert(toDo2 != null);
        System.assertEquals(toDo2.Description__c, 'description was updated');
    }

    @isTest
    static void testupdateToDoFields() {
        Id recordId = createTestRecord();
        RestRequest request = new RestRequest();
        request.requestUri =
                'https://d09000007zhlleam-dev-ed.lightning.force.com/lightning/r/ToDo__c/'
                        + recordId;
        request.httpMethod = 'PATCH';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf('{"Description__c": "Changed description"}');
        RestContext.request = request;
        // Update Description of existing record to 'Changed description'
        ID thisToDoId = ToDoManager.updateToDoFields();
        // Verify record was updated
        System.assert(thisToDoId != null);
        ToDo__c thisToDo = [
                SELECT Id, 
                        Name, 
                        Description__c 
                FROM ToDo__c 
                WHERE Id = :thisToDoId
        ];
        System.assert(thisToDo != null);
        System.assertEquals(thisToDo.Description__c, 'Changed description');
    }

    // Helper method
    static Id createTestRecord() {
        // Create test record
        ToDo__c toDoTest = new ToDo__c(
                Name = 'Check tasks',
                Description__c = 'Only for legal department',
                Priority__c = 'High',
                RecordTypeId = ToDoNames.TO_DO_BUSINESS_RECORD_TYPE,
                Due_Date__c = DateTime.newInstance(2021, 9, 20, 22, 0, 0));
        insert toDoTest;
        return toDoTest.Id;
    }
}
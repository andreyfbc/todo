public class TodoController {
    @AuraEnabled(cacheable=true)
    public static List<SubTodoWrapper> getToDosForTree() {
      
        List<Todo__c> todoWithSubTodoList = [
            SELECT Id ,
                    Name,
                    Due_Date__c,
                    (SELECT Id,
                        Name
                    FROM Sub_ToDos__r) 
            FROM Todo__c
            WHERE Status__c !='Completed'
            AND Due_Date__c>=TODAY
            WITH SECURITY_ENFORCED 
            ORDER by Due_Date__c ASC
        ];

        List<SubTodoWrapper> todoWrappersList = new List<SubTodoWrapper>();
        for(Todo__c todo : todoWithSubTodoList){
            SubTodoWrapper todoWrapper = new SubTodoWrapper() ; 
            todoWrapper.name = todo.Id;
            todoWrapper.label = todo.Name;
            todoWrapper.isTodo = true;
            todoWrapper.expanded = false;
            List<SubTodoWrapper> subTodosList = new List<SubTodoWrapper>();
            for(Sub_ToDo__c subTodo : todo.Sub_ToDos__r){
                SubTodoWrapper subTodoWrapper = new SubTodoWrapper();
                subTodoWrapper.name =subTodo.Id;
                subTodoWrapper.label =subTodo.Name;
                subTodoWrapper.isTodo = false;
                subTodoWrapper.expanded = false;
                subTodoWrapper.items = new List<SubTodoWrapper>();
                subTodosList.add(subTodoWrapper);
            }
            todoWrapper.items = subTodosList;
            todoWrappersList.add(todoWrapper);            
        }
       return todoWrappersList;
    }

    @AuraEnabled(cacheable=true)
    public static Todo__c getSingleTodo() {
        return [
            SELECT Id, 
                    Name, 
                    Description__c, 
                    RecordTypeId, 
                    Priority__c,
                    PriorityFlag__c,
                    Status__c,
                    Due_Date__c
            FROM Todo__c
            WITH SECURITY_ENFORCED
            LIMIT 1
        ];
    }

    public Class SubTodoWrapper{
        @AuraEnabled
        public String name{get;set;}
        @AuraEnabled
        public String label{get;set;}
        @AuraEnabled
        public Boolean isTodo{get;set;}
        @AuraEnabled
        public Boolean expanded{get;set;}
        @AuraEnabled
        public List<SubTodoWrapper> items{get;set;}        
    }

}
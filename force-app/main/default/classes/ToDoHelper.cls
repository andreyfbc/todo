public with sharing class ToDoHelper {
    public static void addToDoToQueu(List<ToDo__c> ToDos) {
        for (ToDo__c toDo : ToDos) {
            if (toDo.RecordTypeId!=ToDoNames.TO_DO_PERSONAL_RECORD_TYPE) {
                toDo.OwnerId = ToDoNames.QUEUE_RECORD_TYPE_MAP.get(toDo.RecordTypeId);
            }
        }
    }
}

@IsTest
public class TestDataFactory {
    public static TestTodoFactory TODO = new TestTodoFactory();

    public class TestTodoFactory{

        public Todo__c createDummyTodo(String Name, String RecordTypeId, String Priority, Boolean needInsert){
            Todo__c todo = new Todo__c();
            todo.Name = Name;
            todo.RecordTypeId = RecordTypeId;
            todo.Priority__c = Priority;
            if (needInsert) insert todo;
            return todo;
        }

        public List<Todo__c> createDummyListOfTodos(Integer count, String Name, String RecordTypeId, String Priority, Boolean needInsert){
            List<Todo__c> todos = new List<Todo__c>();
            for (Integer i=0; i<count; i++){
                todos.add(createDummyTodo(Name, RecordTypeId, Priority, false));
            }
            if (needInsert) insert todos;
            return todos;
        }
    }
}
global with sharing class CleanOldTodoBatch implements Database.Batchable <sObject>{
   
    global Database.QueryLocator start(Database.BatchableContext info){ 
        String SOQL='SELECT Id FROM ToDo__c ';
        if(test.isRunningTest()){
           SOQL+='WHERE NAME LIKE \'Old%\''; 
        }
        else{
            SOQL+='WHERE CreatedDate < LAST_N_MONTHS:3'; 
        }
        return Database.getQueryLocator(SOQL);
    }

    global void execute(Database.BatchableContext info, List<ToDo__c> scope){
        delete scope;
    }

    global void finish(Database.BatchableContext info){ 
    
    }
    
}
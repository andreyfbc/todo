public with sharing class ToDoCallouts {
    public static HttpResponse makeGetCallout(String externalID) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('callout:First_org/'+externalID);
        request.setHeader('Accept', 'application/json'); 
        request.setHeader('Content-Type','application/json;charset=UTF-8');
        request.setMethod('GET');
        HttpResponse response = http.send(request);

        System.debug('response body =' + response.getBody());

        // If the request is successful, parse the JSON response.
        if (response.getStatusCode() == 200) {
            // Deserializes the JSON string into collections of primitive data types.
            Map<String, Object> todos = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());

                System.debug('Received the following todos:');
                for (Object todo : todos.values()) {
                    System.debug(todo);
                }            
        }
        return response;
    }

    public static HttpResponse makePostCallout(String requestBody) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('callout:First_org/');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        request.setBody(requestBody);
        HttpResponse response = http.send(request);
        // Parse the JSON response
        if (response.getStatusCode() != 201) {
            System.debug('The status code returned was not expected: ' +
                    response.getStatusCode() + ' ' + response.getStatus());
        } else {
            System.debug(response.getBody());
        }
        return response;
    }

    public static HttpResponse makeDeleteCallout(String externalID) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setHeader('Accept', 'application/json'); 
        request.setHeader('Content-Type','application/json;charset=UTF-8');
        request.setEndpoint('callout:First_org/'+externalID);

        request.setMethod('DELETE');
        HttpResponse response = http.send(request);

        // If the request is successful, parse the JSON response.
        if (response.getStatusCode() == 200) {
            System.debug('The todo ' + externalID + 'was successfully deleted');
        } else {
            System.debug('The todo was not deleted. The status code: ' +
                    response.getStatusCode() + ' ' + response.getStatus());
        }

        return response;
    }

    public static HttpResponse makePatchCallout(String externalID, String requestBody) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('callout:First_org/'+externalID + '?_HttpMethod=PATCH');
        request.setMethod('POST');
        request.setHeader('X-HTTP-Method-Override', 'PATCH');
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');

        request.setBody(requestBody);
        HttpResponse response = http.send(request);

        // If the request is successful, parse the JSON response.
        if (response.getStatusCode() == 200) {
            System.debug('The todo ' + externalID + 'was successfully updated');
        } else {
            System.debug('The todo was not updated. The status code: ' +
                    response.getStatusCode() + ' ' + response.getStatus());
        }

        return response;
    }

    public static HttpResponse makePutCallout(String externalID, String requestBody) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('callout:First_org/');
        request.setMethod('PUT');
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        request.setBody(requestBody);
        HttpResponse response = http.send(request);
        // Parse the JSON response
        if (response.getStatusCode() != 201) {
            System.debug('The status code returned was not expected: ' +
                    response.getStatusCode() + ' ' + response.getStatus());
        } else {
            System.debug(response.getBody());
        }
        return response;
    }
}
@isTest
public with sharing class CreateTodoFrmEmailTest {
    static testMethod void RecordTypeBusiness() 
    {
              
       Messaging.InboundEmail email = new Messaging.InboundEmail() ;
       Messaging.InboundEnvelope env    = new Messaging.InboundEnvelope();
    
       email.PlainTextBody = 'Record Type: Business Title: Test creation from email Priority: Medium Status: In progress';
       
       email.subject = 'Create Todo';

       env.fromAddress = 'test@test.com';
       CreateTodoFrmEmail obj= new CreateTodoFrmEmail();
       obj.handleInboundEmail(email, env );
                            
    }
    static testMethod void RecordTypePersonal() 
    {
             
       Messaging.InboundEmail email = new Messaging.InboundEmail() ;
       Messaging.InboundEnvelope env    = new Messaging.InboundEnvelope();
    
       email.PlainTextBody = 'Record Type: Personal Title: Test creation from email Priority: Medium Status: In progress';
       
       email.subject = 'Create Todo';

       env.fromAddress = 'test@test.com';
       CreateTodoFrmEmail obj= new CreateTodoFrmEmail();
       obj.handleInboundEmail(email, env );
                            
    }
    static testMethod void RecordTypeCorporate() 
    {
             
       Messaging.InboundEmail email = new Messaging.InboundEmail() ;
       Messaging.InboundEnvelope env    = new Messaging.InboundEnvelope();
    
       email.PlainTextBody = 'Record Type: Corporate Title: Test creation from email Priority: Medium Status: In progress';
       
       email.subject = 'Create Todo';

       env.fromAddress = 'test@test.com';
       CreateTodoFrmEmail obj= new CreateTodoFrmEmail();
       obj.handleInboundEmail(email, env );
                            
    }
    static testMethod void HandlerTest() 
    {
       System.assertEquals(true, myHandler()); 
    }
}

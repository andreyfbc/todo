@isTest
public with sharing class CleanOldTodoTest {
    @isTest
    static void testMethod1()  
    { 
        List<ToDo__c> lstTodo= new List<ToDo__c>(); 
        for(Integer i=0 ;i <10;i++) 
        { 
            ToDo__c todo = new ToDo__c(); 
            todo.Name ='Old'+i;
            lstTodo.add(todo); 
        } 
         
        insert lstTodo; 
         
        Test.startTest(); 
 
        String CRON_EXP = '0 0 0 3 9 ? 2022';
        String jobId = System.schedule('CleanOldTest', CRON_EXP, new CleanOldTodoSchedule());
              
        Test.stopTest(); 
        List<ToDo__c> secondTodo = [SELECT Id FROM ToDo__c];
        System.debug(secondTodo);
    } 
}
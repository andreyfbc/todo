declare module "@salesforce/apex/TodoController.getToDosForTree" {
  export default function getToDosForTree(): Promise<any>;
}
declare module "@salesforce/apex/TodoController.getSingleTodo" {
  export default function getSingleTodo(): Promise<any>;
}
